import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    //:page? --> for the number of page, the '?' avoid errors in case we dont have this page number.
    path: '/locations/:page?',
    alias: '/',
    name: 'Locations',
    // route lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "locations" */ '../views/Locations.vue')
  },
  {
    path: '/episodes/:page?',
    name: 'Episodes',
    component: () => import(/* webpackChunkName: "episodes" */ '../views/Episodes.vue')
  },
  {
    path: '/characters/:page?',
    name: 'Characters',
    component: () => import(/* webpackChunkName: "characters" */ '../views/Characters.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
